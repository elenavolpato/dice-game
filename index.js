// variables for the game state
let scoreP1 = 0
let scoreP2 = 0
let player1Turn = true

// variables to store references to the necessary DOM nodes
const message = document.getElementById("message")
const player1 = document.getElementById("player1Scoreboard")
const player2 = document.getElementById("player2Scoreboard")
const diceP1 = document.getElementById("player1Dice")
const diceP2 = document.getElementById("player2Dice")
const diceTurn = document.getElementsByClassName("active")
const rollBtn = document.getElementById("rollBtn")
const resetBtn = document.getElementById("resetBtn")
const doubleBtn = document.getElementById("doubleBtn")

let diceNum = 0

//randomize which player begins
function begin() { 
  let num = Math.floor(Math.random() * 2) + 1
  if (num != 1) {
    player1Turn = false
    message.textContent = "Player 2 starts"
  } else {
    message.textContent = "Player 1 starts"
  }
}
begin()

// picks a random number from 1 to 6
function rollDice(){
  diceNum = Math.floor(Math.random() * 6) + 1
}

//changes classes, changes text message on top, puts the img of the dice according to number rolled
function shiftTurn() { 
  if (player1Turn) {
    diceP2.classList.remove("active")
    diceP1.classList.add("active")
    message.textContent = "Player 2 Turn"
    diceP1.innerHTML = `<img src="images/${diceNum}.png" class="dice-img"/>`

  } else {
    diceP1.classList.remove("active")
    diceP2.classList.add("active")
    message.textContent = "Player 1 Turn"
    diceP2.innerHTML = `<img src="images/${diceNum}.png" class="dice-img"/>`
  }
  player1Turn = !player1Turn
}

//roll dice button functions
rollBtn.addEventListener("click", function () { 
  rollDice()
  if (player1Turn) {
    scoreP1 += diceNum
    player1.innerText = scoreP1
    shiftTurn()

  } else {
    scoreP2 += diceNum
    player2.innerText = scoreP2
    shiftTurn()
  }
  
  winner()
})

//sets winner messages and makes the reset button appear
function winner() {
  if (scoreP1 >= 20 || scoreP2 >= 20) {
    doubleBtn.style.display = "none"
    rollBtn.style.display = "none"
    resetBtn.style.display = "flex"
    if (scoreP1 > scoreP2) {
      message.textContent = "Player 1 won this game!! 🎉"
    }
    else {
      message.textContent = "Player 2 won this game!! 🥳"
    }
  }
}
//reset button appears after one player reaches 20
resetBtn.addEventListener("click", function () { 
  diceNum = 0
  scoreP1 = diceNum
  scoreP2 = diceNum
  player1.innerText = scoreP1
  diceP1.innerText = "-"
  player2.innerText = scoreP2
  diceP2.innerText = "-"
  doubleBtn.style = "display:flex"
  rollBtn.style = "display:flex"
  resetBtn.style = "display:none"
  begin()
})

//double or nothing button function
doubleBtn.addEventListener("click", function () {
  //selects 1 or 2
  let num = Math.floor(Math.random() * 2) + 1 
  //if 2, doubles the number rolled on the dice
  rollDice()
  if (num === 2) {
    if (player1Turn) {
      scoreP1 += (diceNum * 2) 
      player1.innerHTML = `<span> ${scoreP1} </br> DOUBLED! </span>`
      shiftTurn()
    } else {
      scoreP2 += (diceNum * 2) 
      player2.innerHTML  = `<span> ${scoreP2} </br> DOUBLED! </span>`
      shiftTurn()
    }
  } else {
    if(player1Turn){
      player1.innerHTML = `<span> ${scoreP1} </br> Nothing... </span>`
      shiftTurn()
    } else {
      player2.innerHTML = `<span> ${scoreP2} </br> Nothing... </span>`
      shiftTurn()
    }
    
    
  }
  
  winner()
})