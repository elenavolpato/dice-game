A simple dice game made with vanilla JS. It is one of the challenges of the Frontend Career Path from [scrimba.com](https://scrimba.com/).
The icons used for the dice are _Dice Roll by ainul muttaqin from [NounProject.com](https://thenounproject.com/)_

You can see it working [here](https://elenavolpato.gitlab.io/dice-game/)